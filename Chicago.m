(* ::Package:: *)

(* ::Title:: *)
(*Chicago PlotTheme*)


(* ::Text:: *)
(*Rhys G. Povey*)
(*www.rhyspovey.com*)


(* ::Section:: *)
(*Front End*)


BeginPackage["Chicago`"];


Chicago`SkyBlue=RGBColor@@({135,206,235}/255);


Chicago`RedStar=Graphics[{Red,Polygon[
	{{1/4 (-1+Sqrt[3]),1/4 (3-Sqrt[3])},{0,1},{1/4 (1-Sqrt[3]),1/4 (3-Sqrt[3])},{-(Sqrt[3]/2),1/2},{1/2 (1-Sqrt[3]),0},{-(Sqrt[3]/2),-(1/2)},{1/4 (1-Sqrt[3]),1/4 (-3+Sqrt[3])},{0,-1},{1/4 (-1+Sqrt[3]),1/4 (-3+Sqrt[3])},{Sqrt[3]/2,-(1/2)},{1/2 (-1+Sqrt[3]),0},{Sqrt[3]/2,1/2}}
]}];


(* ::Section:: *)
(*Back End*)


Begin["Private`"];


Themes`AddThemeRules["Chicago",
PlotMarkers->{Chicago`RedStar,0.075},
PlotStyle->Directive[Chicago`SkyBlue,Thickness[0.01]]
];


(* ::Section:: *)
(*End*)


End[];


EndPackage[];
